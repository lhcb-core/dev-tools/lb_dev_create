from lb_dev_create.commands.create import create
from lb_dev_create.builder.package_builder import PackageBuilder

from unittest.mock import patch, Mock


def mock_empty_function(*args, **kwargs):
    print('nothing to see here')

@patch('lb_dev_create.commands.create.PackageBuilder')
def test_create(mock_package_builder):
    package_builder_mock = Mock()
    attrs = {
        'build_directory': mock_empty_function,
        'build_pyproject_file': mock_empty_function,
        'build_readme': mock_empty_function,
        'build_package_directory': mock_empty_function,
        'build_test_directory': mock_empty_function
    }
    package_builder_mock.configure_mock(**attrs)

    mock_package_builder.return_value = package_builder_mock
    
    create('package_directory', False, 'package_name', '0.0.1', 'some_description', ['me'], 'GPL3', 'README.rst', '3.5', None, None, None, [], [], None)

    assert mock_package_builder.build_directory.called_with()
    assert mock_package_builder.build_pyproject_file.called_with('0.0.1', 'some_description', ['me'], 'GPL3', 'README.rst', '3.5', None, None, None, [], [], None)
    assert mock_package_builder.build_directory.called_with('README.rst')
    assert mock_package_builder.build_package_directory.called_with(False)
    assert mock_package_builder.build_test_directory.called_with()