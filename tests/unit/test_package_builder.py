from unittest.mock import patch, Mock

from lb_dev_create.builder.package_builder import PackageBuilder

def mock_mkdir(*args, **kwargs):
    print('creating directory')

def mock_touch(*args, **kwargs):
    print('touching file')

def mock_empty_function(*args, **kwargs):
    print('nothing to see here')

@patch('lb_dev_create.builder.package_builder.Path')
def test_build_directory(mock_path):
    path_mock = Mock()
    attrs = {
        'mkdir': mock_mkdir,
    }
    path_mock.configure_mock(**attrs)

    mock_path.return_value = path_mock
    
    package_builder = PackageBuilder('some_directory_name', 'some_pacakge_name')

    package_builder.build_directory()

    mock_path.assert_called_with('some_directory_name')

@patch('lb_dev_create.builder.package_builder.Path')
def test_build_package_directory(mock_path):
    path_mock = Mock()
    attrs = {
        'mkdir': mock_mkdir,
        'touch': mock_touch
    }
    path_mock.configure_mock(**attrs)

    mock_path.return_value = path_mock
    
    package_builder = PackageBuilder('some_directory_name', 'some_pacakge_name')

    package_builder.build_package_directory(False)

    assert mock_path.call_count == 2

    mock_path.assert_called_with('some_directory_name/some_pacakge_name/__init__.py')

@patch('lb_dev_create.builder.package_builder.Path')
def test_build_package_directory_with_src(mock_path):
    path_mock = Mock()
    attrs = {
        'mkdir': mock_mkdir,
        'touch': mock_touch
    }
    path_mock.configure_mock(**attrs)

    mock_path.return_value = path_mock
    
    package_builder = PackageBuilder('some_directory_name', 'some_pacakge_name')

    package_builder.build_package_directory(True)

    assert mock_path.call_count == 2

    mock_path.assert_called_with('some_directory_name/src/some_pacakge_name/__init__.py')

@patch('lb_dev_create.builder.package_builder.Path')
def test_build_readme(mock_path):
    path_mock = Mock()
    attrs = {
        'touch': mock_touch
    }
    path_mock.configure_mock(**attrs)

    mock_path.return_value = path_mock
    
    package_builder = PackageBuilder('some_directory_name', 'some_pacakge_name')

    package_builder.build_readme('README.rst')

    assert mock_path.call_count == 1

    mock_path.assert_called_with('some_directory_name/README.rst')

@patch('lb_dev_create.builder.package_builder.Path')
def test_build_test_directory(mock_path):
    path_mock = Mock()
    attrs = {
        'mkdir': mock_mkdir,
        'touch': mock_touch
    }
    path_mock.configure_mock(**attrs)

    mock_path.return_value = path_mock
    
    package_builder = PackageBuilder('some_directory_name', 'some_pacakge_name')

    package_builder.build_test_directory()

    assert mock_path.call_count == 3

    mock_path.assert_called_with('some_directory_name/tests/test_some_pacakge_name.py')

@patch('lb_dev_create.builder.package_builder.Path')
@patch('lb_dev_create.builder.package_builder.PyprojectBuilder')
@patch('lb_dev_create.builder.package_builder.FileUtils')
def test_build_pyproject_file(mock_file_utils, mock_pyproject_builder, mock_path):
    pyproject_builder_mock = Mock()
    attrs = {
        'build_package_info': mock_empty_function,
        'build_dependencies': mock_empty_function,
        'build_build_system': mock_empty_function,
        'get_pyproject_content': mock_empty_function
    }
    pyproject_builder_mock.configure_mock(**attrs)

    mock_pyproject_builder.return_value = pyproject_builder_mock

    path_mock = Mock()
    attrs = {
        'mkdir': mock_mkdir,
        'touch': mock_touch
    }
    path_mock.configure_mock(**attrs)

    mock_path.return_value = path_mock
    
    package_builder = PackageBuilder('some_directory_name', 'some_pacakge_name')

    package_builder.build_pyproject_file(None, None, None, None, None, None, None, None, None, None, None, None, )

    assert mock_pyproject_builder.called
    assert mock_file_utils.create_file.called