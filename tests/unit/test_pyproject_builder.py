from lb_dev_create.builder.pyproject_builder import PyprojectBuilder


def test_build_build_system():
    pyproject_builder = PyprojectBuilder()

    pyproject_builder.build_build_system(None)

    expected_result = ['[build_system]', 
                        'requires = [', 
                        '\t"setuptools >= 35.0.2",',
                        '\t"setuptools_scm >= 2.0.0, <3",', 
                        ']', 
                        'build-backend = "setuptools.build_meta"']

    assert pyproject_builder.package_build_system == expected_result

def test_build_build_system_with_backend():
    pyproject_builder = PyprojectBuilder()

    pyproject_builder.build_build_system('some build backend')

    expected_result = ['[build_system]', 
                        'requires = [', 
                        '\t"setuptools >= 35.0.2",',
                        '\t"setuptools_scm >= 2.0.0, <3",', 
                        ']', 
                        'build-backend = "some build backend"']

    assert pyproject_builder.package_build_system == expected_result

def test_build_dependencies():
    pyproject_builder = PyprojectBuilder()

    pyproject_builder.build_dependencies('3.5')

    expected_result = ['[tool.poetry.dependencies]', 
                        'python = "^3.5"']
                        
    assert pyproject_builder.package_dependencies == expected_result

def test_build_package_info():
    pyproject_builder = PyprojectBuilder()

    pyproject_builder.build_package_info('some name', 'some version', 'some description', ['some author'], 'some license', 'some readme', None, None, None, None, None)

    expected_result = [
        '[tool.poetry]',
        'name = "some name"',
        'version = "some version"',
        'description = "some description"',
        'authors = [',
        '\t"some author",',
        ']',
        'license = "some license"',
        'readme = "some readme"'
    ]

    assert pyproject_builder.package_info == expected_result

def test_build_package_info_no_nones():
    pyproject_builder = PyprojectBuilder()

    pyproject_builder.build_package_info('some name', 'some version', 'some description', ['some author'], 'some license', 'some readme', 'some homepage', 'some repository', 'some documentation', ['keyword1', 'keyword2'], ['classifier1', 'classifier2'])

    expected_result = [
        '[tool.poetry]',
        'name = "some name"',
        'version = "some version"',
        'description = "some description"',
        'authors = [',
        '\t"some author",',
        ']',
        'license = "some license"',
        'readme = "some readme"',
        'homepage = "some homepage"', 
        'repository = "some repository"', 
        'documentation = "some documentation"', 
        'keywords = [',
        '\t"keyword1",',
        '\t"keyword2",',
        ']',
        'classifiers = [',
        '\t"classifier1",',
        '\t"classifier2",',
        ']'
    ]

    assert pyproject_builder.package_info == expected_result

def test_get_pyproject_content():
    pyproject_builder = PyprojectBuilder()

    expected_result = '''
[tool.poetry]
name = "some name"
version = "some version"
description = "some description"
authors = [
\t"some author",
]
license = "some license"
readme = "some readme"
homepage = "some homepage"
repository = "some repository"
documentation = "some documentation"
keywords = [
\t"keyword1",
\t"keyword2",
]
classifiers = [
\t"classifier1",
\t"classifier2",
]

[tool.poetry.dependencies]
python = "^3.5"

[tool.poetry.dev-dependencies]

[build_system]
requires = [
\t"setuptools >= 35.0.2",
\t"setuptools_scm >= 2.0.0, <3",
]
build-backend = "setuptools.build_meta"
    '''.strip()

    pyproject_builder.build_package_info('some name', 'some version', 'some description', ['some author'], 'some license', 'some readme', 'some homepage', 'some repository', 'some documentation', ['keyword1', 'keyword2'], ['classifier1', 'classifier2'])

    pyproject_builder.build_dependencies('3.5')

    pyproject_builder.build_build_system(None)


    assert pyproject_builder.get_pyproject_content() == expected_result