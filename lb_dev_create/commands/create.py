from pathlib import Path
import logging

from lb_dev_create.builder.package_builder import PackageBuilder
from lb_utils.string_utils import StringUtils

log = logging.getLogger(__name__)


def create(package, src, name, version, description, authors, license, readme, python, homepage, repository, documentation, keywords, classifiers, build_backend):
    log.debug('`create` command called with [package: {}, src: {}, name: {}, version {}, description {}, authors {}, license {}, readne {}, python {}, homepage {}, repository {}, documentation {}, keywords {}, classifiers {}, build_backend {}]'.format(
        package, src, name, version, description, authors, license, readme, python, homepage, repository, documentation, keywords, classifiers, build_backend))
    print('creating package...')
    log.info('creating package')

    log.debug('replacing \'-\' with \'_\'')
    if name:
        name = StringUtils.make_package_compatible(name)

    package = StringUtils.make_package_compatible(package)
    package_name = name or package

    package_builder = PackageBuilder(package, package_name)

    package_builder.build_directory()

    package_builder.build_pyproject_file(version, description, authors, license, readme, python, homepage, repository, documentation, keywords, classifiers, build_backend)

    package_builder.build_readme(readme)

    package_builder.build_package_directory(src)

    package_builder.build_test_directory()