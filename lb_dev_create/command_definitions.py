import click
import logging

from lb_utils.log_utils import set_up_logging
from lb_dev_create.commands.create import create

log = logging.getLogger(__name__)

@click.command(name='create', help='Creates a new package skeleton')
@click.argument('package')
@click.option('--src', is_flag=True, help='create src folder')
@click.option('--name', help='differentiates package name with folder name')
@click.option('--version', default='0.0.1', help='the initial version of the project')
@click.option('--description', default='<Please insert a description>', help='package description')
@click.option('--authors', '-a', default=['CERN - LHCb Core Software <lhcb-core-soft@cern.ch>'], multiple=True, help='author of the project')
@click.option('--license', '-l', default='GPL-3.0-or-later', help='the license of the project')
@click.option('--readme', default='README.rst', help='the name of the readme file')
@click.option('--python', '-p', default='*', help='python versions supported by this project')
@click.option('--homepage', help='the homepage link of the package')
@click.option('--repository', help='the repository link of the package')
@click.option('--documentation', help='the documentation link of the package')
@click.option('--keywords', multiple=True, help='keywords of the project')
@click.option('--classifiers', multiple=True, help='classifiers of the project')
@click.option('--build-backend', help='backend to use for build')
@click.option('-v', '--verbose', count=True)
def create_command(package, src, name, version, description, authors, license, readme, python, homepage, repository, documentation, keywords, classifiers, build_backend, verbose):
    set_up_logging(verbose)
    create(package, src, name, version, description, authors, license, readme, python, homepage, repository, documentation, keywords, classifiers, build_backend)