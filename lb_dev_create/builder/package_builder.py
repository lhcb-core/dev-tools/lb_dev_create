from pathlib import Path
import logging

from lb_dev_create.builder.pyproject_builder import PyprojectBuilder
from lb_utils.file_utils import FileUtils

log = logging.getLogger(__name__)

class PackageBuilder:

    def __init__(self, directory_name, package_name):
        self.directory_name = directory_name
        self.package_name = package_name

    def build_directory(self):
        log.info('creating directory {}'.format(self.directory_name))
        Path(self.directory_name).mkdir(parents=True, exist_ok=True)

    def build_readme(self, readme):
        log.info('creating {}'.format(readme))
        Path('{}/{}'.format(self.directory_name, readme)).touch(exist_ok=True)

    def build_package_directory(self, has_src):
        if has_src:
            log.info('creating package directory in src/{}'.format(self.package_name))
            Path(
                '{}/src/{}'.format(self.directory_name, self.package_name)
            ).mkdir(parents=True, exist_ok=True)
            Path(
                '{}/src/{}/__init__.py'.format(self.directory_name,self.package_name)
            ).touch(exist_ok=True)

        else:
            log.info('creating package directory in {}'.format(self.package_name))
            Path(
                '{}/{}'.format(self.directory_name, self.package_name)
            ).mkdir(parents=True, exist_ok=True)
            
            Path(
                '{}/{}/__init__.py'.format(self.directory_name,self.package_name)
            ).touch(exist_ok=True)

    def build_test_directory(self):
        log.info('creating test directory')
        Path('{}/tests'.format(self.directory_name)).mkdir(parents=True, exist_ok=True)

        Path('{}/tests/__init__.py'.format(self.directory_name)).touch(exist_ok=True)

        Path('{}/tests/test_{}.py'.format(self.directory_name, self.package_name)).touch(exist_ok=True)

    def build_pyproject_file(self, version, description, authors, license, readme, python, homepage, repository, documentation, keywords, classifiers, build_backend):
        log.info('creating pyproject file')
        
        Path('{}/pyproject.toml'.format(self.directory_name)).touch(exist_ok=True)

        pyproject_builder = PyprojectBuilder()

        pyproject_builder.build_package_info(self.package_name, version, description, authors, license, readme, homepage, repository, documentation, keywords, classifiers)

        pyproject_builder.build_dependencies(python)

        pyproject_builder.build_build_system(build_backend)

        pyproject_content = pyproject_builder.get_pyproject_content()

        FileUtils.create_file('{}/pyproject.toml'.format(self.directory_name), pyproject_content, overwrite=True)
