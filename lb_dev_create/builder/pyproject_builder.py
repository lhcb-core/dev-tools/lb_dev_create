import logging

log = logging.getLogger(__name__)

class PyprojectBuilder:

    def __init__(self):
        self.package_info = []
        self.package_dependencies = []
        self.package_dev_dependencies = ['[tool.poetry.dev-dependencies]']
        self.package_build_system = []

        self.name = ''
        self.version = ''
        self.description = ''
        self.authors = ''
        self.license = ''
        self.readme = ''
        self.python = ''
        self.homepage = ''
        self.repository = ''
        self.documentation = ''
        self.keywords = []
        self.classifiers = []

    def build_package_info(self, package_name, version, description, authors, license, readme, homepage, repository, documentation, keywords, classifiers):
        log.info('setting up package information')
        package_info = ['[tool.poetry]']
        package_info.append('name = "{}"'.format(package_name))

        package_info.append('version = "{}"'.format(version))
        
        package_info.append('description = "{}"'.format(description))
        
        package_info.append('authors = [')
        for author in authors: package_info.append('\t"{}",'.format(author))
        package_info.append(']')
        
        package_info.append('license = "{}"'.format(license))
        
        package_info.append('readme = "{}"'.format(readme))

        if homepage:
            package_info.append('homepage = "{}"'.format(homepage))

        if repository:
            package_info.append('repository = "{}"'.format(repository))

        if documentation:
            package_info.append('documentation = "{}"'.format(documentation))

        if keywords:
            package_info.append('keywords = [')
            for keyword in keywords: package_info.append('\t"{}",'.format(keyword))
            package_info.append(']')

        if classifiers:
            package_info.append('classifiers = [')
            for classifier in classifiers: package_info.append('\t"{}",'.format(classifier))
            package_info.append(']')

        self.package_info = package_info

    def build_dependencies(self, python):
        log.info('setting up dependencies')
        package_dependencies = ['[tool.poetry.dependencies]']
        package_dependencies.append('python = "^{}"'.format(python))

        self.package_dependencies = package_dependencies

    def build_build_system(self, build_backend):
        log.info('setting up build system')
        package_build_system = ['[build_system]']
        package_build_system.append('requires = [')
        package_build_system.append('\t"setuptools >= 35.0.2",')
        package_build_system.append('\t"setuptools_scm >= 2.0.0, <3",')
        package_build_system.append(']')

        if build_backend:
            package_build_system.append('build-backend = "{}"'.format(build_backend))
        else:
            package_build_system.append('build-backend = "setuptools.build_meta"')

        self.package_build_system = package_build_system

    def get_pyproject_content(self):
        log.info('putting everything together')
        package_info = '\n'.join(self.package_info)
        package_dependencies = '\n'.join(self.package_dependencies)
        package_dev_dependencies = '\n'.join(self.package_dev_dependencies)
        package_build_system = '\n'.join(self.package_build_system)

        pyproject_content = '\n\n'.join([package_info, package_dependencies, package_dev_dependencies, package_build_system])

        return pyproject_content